# Uncertainty Quantification of Reactivity Scales

Dear user,

We present a novel approach to calculating bimolecular rate constants of polar organic reactions as well as Mayr-type reactivity parameters of electrophiles and nucleophiles, which is rooted in uncertainty quantification.
It transforms the *unique* values of those quantities into value *distributions*.
This extension allows users of our code to perform *virtual* measurements of polar organic reactivity, which are reported as expectation +/- deviation (95% confidence interval), just like *physical* measurements.

This repository represents an expanded Supporting Information to the following publication:

---
### J. Proppe, J. Kircher, Uncertainty Quantification of Reactivity Scales, *ChemRxiv* **2021**, https://doi.org/10.33774/chemrxiv-2021-hwh2d-v2

---

**Please cite the above-mentioned reference when publishing results generated with the code/notebooks provided by this repository, also if you post-processed them.**

The `MayrUQ` class (the actual code) can be found in the `main.py` file of the `mayruq/` directory.
The `tutorial.ipynb` notebook introduces the data set as well as the model/code for optimizing reactivity parameters.
The `uq.ipynb` notebook allows users to visualize the results of our uncertainty quantification study.

**Should you be interested in an interactive version of this repository, visit https://tinyurl.com/mayruq-jupyter-v1 and run `tutorial.ipynb` followed by `uq.ipynb`.**

If you would like to provide us with feedback, report technical problems, etc., please contact me at j.proppe@tu-braunschweig.de.

Jonny Proppe, 
4 Jul 2021
(updates: 25 Nov 2021)
